package jp.alhinc.sadatou_takanori.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		// コマンドライン引数のエラー表示
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}

		// 支店定義ファイルの読み込みなどに使用
		Map<String, String> branchName = new HashMap<String, String>();

		// 商品定義ファイルの読み込みなどに使用
		Map<String, String> commodityName = new HashMap<String, String>();

		// 売り上げファイルの読み込みなどに使用
		Map<String, Long> branchSales = new HashMap<String, Long>();

		// 商品別売り上げファイルの読み込みなどに使用
		Map<String, Long> commoditySales = new HashMap<String, Long>();

		// ファイルの情報を処理するための呼び出し元関数(売り上げ集計ファイル)
		if (!inputFile(args[0], "branch.lst", branchName, branchSales, "支店", "^([0-9]{3})$")) {
			return;
		}

		// ファイルの情報を処理するための呼び出し元関数(商品別売り上げ集計ファイル)
		if (!inputFile(args[0], "commodity.lst", commodityName, commoditySales, "商品", "^([a-zA-Z0-9]{8})$")) {
			return;
		}

		// 全てのファイルを取得
		File[] files = new File(args[0]).listFiles();

		// ファイルの情報を格納するためListを宣言
		// 売り上げファイルだけ格納する
		List<File> rcdFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {

			// ファイル名が0～9で始まり、かつ8桁で、末尾がrcdで終わる場合
			String rcdFileName = files[i].getName();
			if (rcdFileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}

		}

		// 売上ファイルが連番になっていない場合エラー表示する
		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			// 比較元(i)
			// ファイルを一つ取ってくる
			File target = rcdFiles.get(i);
			// ファイルの名前を取ってくる
			String fileName = target.getName();
			// 拡張子「.rcd」を含めない
			String numberForStr = fileName.substring(0, 8);
			// String型の数字からInt型の数字に変換
			int numberForInt = Integer.parseInt(numberForStr);

			// 比較先(i+1)
			// ファイルを一つ取ってくる
			File another = rcdFiles.get(i + 1);
			// ファイルの名前をとってくる
			String name = another.getName();
			// 拡張子「.rcd」を含めない
			String number = name.substring(0, 8);
			// String型の数字からInt型の数字に変換
			int change = Integer.parseInt(number);

			// 連番になっていない場合(差分が1ではない場合)
			if (change - numberForInt != 1) {
				System.out.println("売上ファイル名が連番になっていません。");
				return;
			}
		}

		// 売上ファイルを読み込み、中身を確認
		// 売り上げファイルの情報の中身を格納するためListを宣言
		for (int i = 0; i < rcdFiles.size(); i++) {
			List<String> fileContents = new ArrayList<String>();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				String row = "";
				while ((row = br.readLine()) != null) {
					fileContents.add(row);

				}
				// 売上ファイルの中身確認、3行なのか
				if (fileContents.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です。");
					return;

				}
				// 売上ファイルの売上金額が数字ではなかった場合
				if (!fileContents.get(2).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生いたしました。");
					return;
				}

				// longに置き換え、売上金額を取り出す
				long line = Long.parseLong(fileContents.get(2));

				// longに置き換え、商品の売上金額を取り出す
				long commodityLine = Long.parseLong(fileContents.get(2));

				// 売上ファイルの支店コードが支店定義ファイルに該当しなかった場合
				if (!branchName.containsKey(fileContents.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です。");
					return;
				}
				// 売上ファイルの商品コードが商品定義ファイルに該当しなかった場合
				if (!commodityName.containsKey(fileContents.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です。");
					return;
				}
				// Mapから取り出す支店コード
				String branchCord = (fileContents.get(0));
				// Mapから取り出す商品コード
				String commodityCord = (fileContents.get(1));
				// 取り出したString型の支店コードをKeyとしてMapの値にlong型の売上金額を取得する
				long sale = (branchSales.get(branchCord));
				// 取り出したString型の商品コードをKeyとしてMapの値にlong型の商品の売上金額を取得する
				long commoditySale = (commoditySales.get(commodityCord));

				// 売上金額と既にマップにある売上金額を加算する
				long sum = line + sale;

				// 商品の売上金額と既にマップにある商品の売上金額を加算する
				long commoditySum = commodityLine + commoditySale;

				// 集計した売上金額が10桁を超えた場合
				if (sum >= 1000000000L) {
					System.out.println("合計金額が10桁を超えました。");
					return;
				}

				// 集計した商品の売上金額が10桁を超えた場合
				if (commoditySum >= 1000000000L) {
					System.out.println("合計金額が10桁を超えました。");
					return;
				}

				// String型の支店コードをKeyに、売り上げ合計額をlong型のValueとしてbranchSalesマップにput
				branchSales.put(branchCord, sum);

				// String型の商品コードをKeyに、商品の売り上げ合計額をlong型のValueとしてcommoditySalesマップにput
				commoditySales.put(commodityCord, commoditySum);

			} catch (IOException error) {
				System.out.println("予期せぬエラーが発生しました。");
				return;

			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
						return;
					}
				}
			}
		}

		// ファイルの出力を処理するための呼び出し元関数(売り上げ集計ファイル)
		if (!outputFile(args[0], "branch.out", branchName, branchSales)) {
			return;
		}
		// ファイルの出力を処理するための呼び出し元関数(商品別売り上げ集計ファイル)
		if (!outputFile(args[0], "commodity.out", commodityName, commoditySales)) {
			return;
		}
	}

	// ファイルの情報を処理するための呼び出し先関数
	public static boolean inputFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> branchSales, String fileNameIndex, String fileFormat) {

		// 支店・商品定義ファイル読み込みの初期化
		BufferedReader br = null;

		try {

			// 支店・商品定義ファイルの指定
			File file = new File(path, fileName);

			// 支店・商品定義ファイルエラー処理
			if (!file.exists()) {
				System.out.println(fileNameIndex + "定義ファイルが存在しません。");
				return false;
			}

			// 支店・商品定義ファイルの読み込み
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			// 支店・商品定義ファイルの行読み込みのための変数
			String line;
			while ((line = br.readLine()) != null) {

				// 支店定義ファイルの支店コードと支店名をそれぞれ別に保存する
				// 商品定義ファイルの商品コードと商品名をそれぞれ別に保存する
				String[] items = line.split(",");

				// items[0]には支店コードと商品コード、items[1]には支店名と商品名が格納される
				names.put(items[0], items[1]);
				branchSales.put(items[0], 0L);

				/* 配列の要素数が2つでない、
				   もしくは支店コードが数字3桁ではない、もしくは商品コードが英数字8桁ではない場合 */
				if (items.length != 2 || (!items[0].matches(fileFormat))) {
					System.out.println(fileNameIndex + "定義ファイルのフォーマットが不正です。");
					return false;
				}
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}

	// ファイルの出力を処理するための呼び出し先関数
	public static boolean outputFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> branchSales) {

		// 支店別集計ファイル書き込みの初期化
		// 商品別集計ファイル書き込みの初期化
		BufferedWriter bw = null;

		// ファイルを作成
		try {
			// 支店別集計ファイル・商品別集計ファイルの指定
			File file = new File(path, fileName);
			// 支店別集計ファイル・商品別集計ファイルに書き込み
			FileWriter branch = new FileWriter(file);

			bw = new BufferedWriter(branch);

			// 支店コード、支店名、売上金額を改行させて書き込む
			// 商品コード、商品名、合計金額を改行させて書き込む
			for (String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
}
